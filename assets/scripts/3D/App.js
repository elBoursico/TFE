class App {

    constructor( faction, {
        width = window.innerWidth,
        height = window.innerHeight
    } = {}) {

        this.canvasHolder = document.body;

        this.renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });
        this.renderer.setSize(width, height);
        this.renderer.domElement.style.position = "absolute";
        this.canvasHolder.appendChild(this.renderer.domElement);

        this.scene = new THREE.Scene();

        this.camera = new Camera();

        this.ticker =  TweenMax.ticker;
        this.stats = new Stats();

        // this.controls = new THREE.OrbitControls( this.camera, this.canvasHolder );
        // this.controls.addEventListener( 'change', this.update.bind(this));

        this.inputs = new Inputs();

        this.faction = faction;

        if (this.faction === "shadow") {

            this.renderer.setClearColor(0x282828, 1);

        } else {
            this.renderer.setClearColor(0xFFFFFF, 1);
        }

    };

    start (socketManager) {

        const levelController = new LevelController();
        levelController.loadLevel("test");

        this.socketManager = socketManager;

        this.add(levelController);

        this.stats.showPanel(0);
        document.body.appendChild(this.stats.dom);

        if (this.faction === "shadow") {

            console.log("main player shadow");

            const player = new Player(this.inputs, levelController, false, this.socketManager);
            player.setPosition(0,10,0);
            this.add(player);

            const player2 = new Player(this.inputs, levelController, true, this.socketManager);
            player2.setPosition(30,10,0);
            player2.sphere.material.color.setHex(0x1ccf43);
            this.add(player2);

            this.socketManager.emitPosition(player.sphere.position);
            this.socketManager.getPlayer(player2);

        } else {

            console.log("main player light");

            const player = new Player(this.inputs, levelController, true, this.socketManager);
            player.setPosition(0,10,0);
            this.add(player);

            const player2 = new Player(this.inputs, levelController, false, this.socketManager);
            player2.setPosition(30,10,0);
            player2.sphere.material.color.setHex(0x1ccf43);
            this.add(player2);
            this.socketManager.emitPosition(player2.sphere.position);
            this.socketManager.getPlayer(player);

        }

        this.ticker.addEventListener("tick", this.update.bind(this));

        var light = new THREE.AmbientLight( 0x404040, 10 ); // soft white light
        this.add( light );

    }

    // setSize(width,height,fov){
    //     let _fov = fov || 45;
    //     this.renderer.setSize(width,height);
    //     this.camera = new THREE.PerspectiveCamera(_fov,width/height,1,1000);
    // }

    update(){
        this.stats.begin();

        for (let i in this.scene.children) {

            if (this.scene.children[i].update) {

                this.scene.children[i].update();

            }

        }

        App.render.call(this);

        this.stats.end();
    }

    add(object){
        if(object instanceof THREE.Object3D){
            this.scene.add(object);
        }else{
            console.error(object + ' is a '+ typeof object +', should be an instance of THREE.Object3D');
        }
    }

    static render(){
        this.renderer.render(this.scene,this.camera);
    }

    get renderDomElement(){
        return this.renderer.domElement;
    }

}
