class Player extends THREE.Object3D {

    constructor (inputs, levelController, secondPlayerBoolean, socketManager) {

        super();
        const geometry = new THREE.SphereGeometry( 10, 32, 32 );
        geometry.computeBoundingBox();
        const material = new THREE.MeshPhongMaterial({color: 0xcb1010});

        this.sphere = new THREE.Mesh( geometry, material );
        this.add(this.sphere);

        this.inputs = inputs;

        this.levelController = levelController;

        this.inAir = false;
        this.velocityY = 0;
        this.gravity = 0.6;

        this.socketManager = socketManager;

        this.secondPlayer = secondPlayerBoolean;

        this.rays = [
            new THREE.Vector3(1,0,0),
            new THREE.Vector3(-1,0,0),
            new THREE.Vector3(0,1,0),
            new THREE.Vector3(0,-1,0)
        ]

        this.obstacles = levelController.getBorderFromActualLevel();

        this.raycaster = new THREE.Raycaster();

        this.collision;
        this.range = 10;

    }

    setPosition(x,y,z) {

        this.sphere.position.set(x,y,z);

    }

    static collision () {

        this.isColliding = {
            left : false,
            right: false,
            down : false,
            up : false
        }

        for (let i in this.rays) {
            this.raycaster.set(this.sphere.position, this.rays[i]);

            this.collision = this.raycaster.intersectObjects(this.obstacles);

            if (this.collision.length > 0 && this.collision[0].distance <= this.range) {

                if (i == 0) {
                    this.isColliding.right = true;
                } else if ( i == 1) {
                    this.isColliding.left = true;
                } else if (i == 2 ) {
                    this.isColliding.up = true;
                } else if ( i == 3) {
                    this.floorPosition = this.collision[0].point;
                    this.isColliding.down = true;
                }

            }

        }

    }

    update() {

        Player.collision.call(this);

        // TODO: faire en sorte que lorsqu'on appuie sur right alors que on appuie déjà sur left, ben en faite, on va left

        if (this.secondPlayer) {

        } else {

            this.socketManager.emitPosition(this.sphere.position);

            // this.socket.emit("position", this.sphere.position);

            if (this.inputs.leftActive) {

                if (this.isColliding.left) {
                    // une animation indiquant qu'on ne peut pas avancer plus, éventuellement
                } else {
                    Player.moveLeft.call(this);
                }

            }

            if (this.inputs.rightActive) {

                if (this.isColliding.right) {
                    // une animation indiquant qu'on ne peut pas avancer plus, éventuellement
                } else {
                    Player.moveRight.call(this);
                }
            }

            if (this.inAir) {

                this.velocityY -= this.gravity;

                if (this.isColliding.down) {

                    this.velocityY = 0;
                    this.inAir = false;
                    this.sphere.position.y = this.floorPosition.y + 10;

                }

            }

            if (this.isColliding.down) {

                this.inAir = false;

            } else {
                this.inAir = true;
            }

            if (this.inputs.jumpPressed) {

                if (this.inAir) {

                } else {
                    Player.jump.call(this);
                }

            }

            this.sphere.position.y += this.velocityY;

        }

    }

    static moveLeft() {
        this.sphere.position.x -= 1;
    }

    static moveRight() {
        this.sphere.position.x += 1;
    }

    static jump() {
        this.velocityY = 10;
        this.inAir = true;
    }

}
