document.addEventListener("DOMContentLoaded", function() {

    const menuDomElement = {
        menuHome : document.querySelector(".home"),
        menuFaction : document.querySelector(".faction"),
        menuWaiting : document.querySelector(".waiting"),
        buttonPlay : document.querySelector(".play"),
        buttonShadow : document.querySelector(".shadow"),
        buttonLight : document.querySelector(".light")
    }

    const menuController = new MenuController(menuDomElement);

    const socket = new SocketManager(menuController, io);

});
